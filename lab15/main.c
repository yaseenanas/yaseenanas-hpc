# include <stdio.h>
# include <stdlib.h>
# include <math.h>
#ifdef _OPENMP
#  include <omp.h>
#endif


int main(int argc, char* argv[])
{
   void usage(const char* prog_name);
   double CompTrap(const double a,const double b,const int N, const int thread_count);

   if (argc != 3) { usage(argv[0]); }
   const int thread_count = strtol(argv[1], NULL, 10);
   const int N = strtol(argv[2], NULL, 10);
   if (thread_count<1 || N<1 || N % thread_count != 0)
    { usage(argv[0]); }

   const double a = -1.0; const double b = 1.0;
   const double time1= omp_get_wtime();
   double T = CompTrap(a,b,N,thread_count);
   const double time2 = omp_get_wtime();
   const double clock_time = time2-time1;
   const double Iex = 0.62661737464261433833;
   const double err = fabs(T-Iex);

   // printf("\n N = %i, T = %23.15e, err = %12.5e\n\n",N,T,err);
   printf("With %2i threads, clock_time = %11.5e (sec), N = %i, T = %11.15e, err = %11.5e\n\n", thread_count, clock_time, N ,T,err);

   return 0;
}

void usage(const char *prog_name)
{
   fprintf(stderr,"usage: %s <num_threads> <num_intervals>\n",
	   prog_name);
   fprintf(stderr,"   num_threads should be positive\n");
   fprintf(stderr,"   num_intervals should be positive\n");
   fprintf(stderr,"   mod(num_intervals,num_threads) != 0\n");
   exit(1);
}

double CompTrap(const double a, const double b, const int N, const int thread_count)
{
   double func(const double x);
   double h = (b-a)/((double)N);
   double p = 0;
   double q = 0;
   int pmax = (N+1)/2;
   int qmax = (N-1)/2;
   for (int j= 2; j== pmax; j++)
   {
     p = p + func((2*j)-1);
   }

   for (int z = 2; z== qmax; z++)
   {
     q = q + func((2*z) -1);
   }

   double T = 1/3*(func(a)+ 2*q + 4*p +func(b));
   // double x = a;

  # pragma omp parallel for num_threads(thread_count)  \
    reduction(+: T)
   for (int i=1; i<N; i++)
   {
      T += func(a+i*h);
   }

   return h*T;
}

double func(const double x)
{
   return (exp(-8*x*x));
}
