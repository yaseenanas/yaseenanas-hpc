def getdata(infile):
    import numpy as np
    array1=[]
    array2=[]
    with open(infile) as f:
        for line in f:
            item1,item2= map(float,line.split())
            array1.append(item1)
            array2.append(item2)
    x= np.array(array1)
    y= np.array(array2)
    return x,y


def approx(x,y):
    import numpy as np
    from numpy.linalg import solve
    n= len(x)
    A= [1,x[0],x[0]**2]
    B= [y[0]]
    for i in range (1,n):
        xrow= np.array([1,x[i],x[i]**2])
        yrow= np.array([y[i]])
        A= np.vstack([A,xrow])
        B= np.vstack([B,yrow])
    At= np.transpose(A)
    ATA= np.dot(At,A)
    ATB= np.dot(At,B)
    a= np.linalg.solve(ATA,ATB)
    return a

def plotdata(x,y,a,infile):
    import numpy as np
    import matplotlib.pyplot as plt
    yfit= a[0]+a[1]*x+a[2]*x**2
    plt.plot(x,yfit, "-r", label= "Approx")
    plt.plot(x,y, "-y", label= "Actual")
    plt.legend()
    plt.title(infile + ": Data + Least Squares Fit w/ Quad. Poly.")
    plt.savefig(infile+'.png', bbox_inches='tight')

if __name__== "__main__":
    import matplotlib.pyplot as plt
    data1= 'xydata3.dat'
    data2= 'xydata2.dat'
    data3= 'xydata1.dat'
    data4= 'xydata4.dat'
    x1,y1= getdata(data1)
    x2,y2= getdata(data2)
    x3,y3= getdata(data3)
    x4,y4= getdata(data4)
    solve1= approx(x1,y1)
    solve2= approx(x2,y2)
    solve3= approx(x3,y3)
    solve4= approx(x4,y4)
    plt.figure(1)
    plotdata(x1,y1,solve1,data1)
    plt.figure(2)
    plotdata(x2,y2,solve2,data2)
    plt.figure(3)
    plotdata(x3,y3,solve3,data3)
    plt.figure(4)
    plotdata(x4,y4,solve4,data4)
