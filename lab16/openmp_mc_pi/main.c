# include <stdio.h>
# include <stdlib.h>
#include <time.h>
#include "matrix.h"
# include <math.h>
// #define SEED 35791246
#ifdef _OPENMP
#  include <omp.h>
#endif

int main(int argc, char* argv[])
{
  int thread_count = strtol(argv[1], NULL, 10);
  int N = strtol(argv[2], NULL, 10);
  double monte(int N, int thead_count);
  const double time1= omp_get_wtime();
  double piestimate = monte(N, thread_count);
  const double time2= omp_get_wtime();
  const double cpu_time= time2-time1;
  const double pi= 3.14159265358979323846;
  double error = fabs((piestimate-pi)/pi);
  printf("With %2i threads, clock_time = %11.5e (sec), N = %i, error = %11.5e\n\n", thread_count, cpu_time, N ,error);

//Creates txt file
  char filename[]= "mc_pi_table.txt";
  FILE* outfile= fopen(filename, "a+");
  fprintf(outfile,"%15i %15f %15f\n",N,cpu_time,error);
  int fclose(FILE* filename);


  return 0;
}

double monte(const int N, const int thread_count)
{

  double r=0;
  double x;
  double y;
  double xsqr=0;
  double ysqr=0;
  int in_circle= 0;
  double pie;
  srand(time(NULL));
  # pragma omp parallel for num_threads(thread_count) \
    reduction(+: in_circle)
  for (int i=0; i<N; i++)
  {
    x= ((double)rand()/(double)RAND_MAX);
    y= ((double)rand()/(double)RAND_MAX);
    xsqr= pow(x,2);
    ysqr= pow(y,2);
    r= sqrt(xsqr+ysqr);
    if (r<= 1.0)
    {
     in_circle++;
    }
  }

  pie= (double)in_circle/N*4;
  return pie;
}
