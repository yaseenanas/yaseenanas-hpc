#!/bin/bash

# Copy/paste this job script into a text file and submit with the command:
#    sbatch thefilename
# job standard output will go to the file slurm-%j.out (where %j is the job ID)

#SBATCH --time=1:00:00   # walltime limit (HH:MM:SS)
#SBATCH --nodes=1   # number of nodes
#SBATCH --ntasks-per-node=16   # 16 processor core(s) per node 
#SBATCH --mail-user=asyaseen@iastate.edu   # email address
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL

# LOAD MODULES, INSERT CODE, AND RUN YOUR PROGRAMS HERE
./main.exe 16 10 > output.txt
./main.exe 16 100 > output.txt
./main.exe 16 1000 > output.txt
./main.exe 16 10000 > output.txt
./main.exe 16 100000 > output.txt
./main.exe 16 1000000 > output.txt
./main.exe 16 10000000 > output.txt
./main.exe 16 100000000 > output.txt


