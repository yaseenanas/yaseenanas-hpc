def cpu_plot(infile):
    import numpy as np
    import matplotlib.pyplot as plt
    import math
    x= [];
    y= [];
    z= [];

    with open(infile) as f:
        for line in f:
            item1,item2,item3= map(float,line.split())
            x.append(item1)
            y.append(item2)
            z.append(item3)
    N= np.array(x)
    cpu= np.array(y)
    error= np.array(z)
    #initialize plots
    figure, axis = plt.subplots(2)
    #CPU scale with N plot
    axis[0].plot(N,cpu)
    axis[0].set_title("N versus CPU")
    axis[0].set_xlabel("N")
    axis[0].set_ylabel("CPU Time (sec)")

    #Error scale with N plot
    axis[1].plot(N,error)
    axis[1].set_title("N versus Error")
    axis[1].set_xlabel("N")
    axis[1].set_ylabel("Error")

   #Combines both subplots
    plt.savefig("CPU_plot.png", bbox_inches= 'tight')
    plt.show()

    return N,cpu,error

if __name__=="__main__":
    data= 'mc_pi_table.txt'
    plot= cpu_plot(data)
