# include <stdio.h>
# include <stdlib.h>
#include <time.h>
#include "matrix.h"
# include <math.h>
// #define SEED 35791246
#ifdef _OPENMP
#  include <omp.h>
#endif

int main(int argc, char* argv[])
{
  int N = strtol(argv[1], NULL, 10);;
  double monte(int N);
  clock_t time0, time1;

  time0= clock();
  double piestimate = monte(N);
  time1= clock();
  printf("%f\n", piestimate);
  double cpu_time= ((double)(time1-time0))
       /((double)(CLOCKS_PER_SEC));
  printf("CPU time for Conditional Number: %11.5e\n", cpu_time);
  const double pi= 3.14159265358979323846;
  double error = fabs((piestimate-pi)/pi);
  printf("Relative error: %11.6e\n", error);

//Creates txt file
  char filename[]= "mc_pi_table.txt";
  FILE* outfile= fopen(filename, "a+");
  fprintf(outfile,"%15i %15f %15f\n",N,cpu_time,error);
  int fclose(FILE* filename);


  return 0;
}

double monte(const int N)
{

  double r;
  double x;
  double y;
  double xsqr;
  double ysqr;
  int in_circle;
  double pie;
  int a;
  srand(time(NULL));
  for (int i=0; i<N; i++)
  {
    x= ((double)rand()/(double)RAND_MAX);
    y= ((double)rand()/(double)RAND_MAX);
    xsqr= pow(x,2);
    ysqr= pow(y,2);
    r= sqrt(xsqr+ysqr);
    if (r<= 1.0)
    {
     a= in_circle++;
    }
  }

  pie= (double)a/N*4;
  return pie;
}
