#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <time.h>
#include "matrix.h"
#include "trimatrix.h"

int main()
{
  srand( time(NULL) );
  int N=0;
  printf("    Input N: ");
  scanf("%i",&N);
  assert(N>1);

  // Create a random symmetric matrix
  matrix A = new_matrix(N,N);

  // for (int i=1; i<=N; i++)
  //   for (int j=1; j<=i; j++)
  //     {
  //       double tmp = ((double)rand())/((double)RAND_MAX);
  //       tmp = (double)((int)(10000.0*tmp))/10000.0;
  //       mget(A,i,j) = tmp;
      // }
  for (int i=1; i<=N; i++)
    for (int j=1; j<=N; j++)
      { mget(A,i,j) = ((2*N*N)+1)*(i==j)+ -1.0*N*N *(i-1==j) + -1.0*N*N *(j-1==i);}

  mget(A,1,N)= -1.0*N*N;
  mget(A,N,1)= -1.0*N*N;

  //clock times
  clock_t time0, time1, time2, time3;

  // Hessenberg reduction to tridiagonal form
  trimatrix T = new_trimatrix(N);
  time0= clock();
  void Hessenberg(const matrix* A, trimatrix* T);
  Hessenberg(&A,&T);
  time1= clock();
  printf("\n");
  printf("Original Matrix:\n");
  print_matrix(&A);
  printf("Reduction to Tridiagonal Form:\n");
  print_trimatrix(&T);

  // QR Algorithm to find eigenvalues of T
  // which are the same as the eigenvalues of A
  void QRA(trimatrix* T);
  QRA(&T);
  time2= clock();
  printf("After QR Algorithm:\n");
  print_trimatrix(&T);

// Calculates Condition Number
double conditionnumber= ConditionNumber(&T,N);
time3= clock();
printf("The Condition Number for Matrix A is: %f\n", conditionnumber);

//Report times

//Time to do Hessenberg Function
double cpu_time1= ((double)(time1-time0))
       /((double)(CLOCKS_PER_SEC));

//Time to do QR Function
double cpu_time2= ((double)(time2-time1))
       /((double)(CLOCKS_PER_SEC));

//Time to do Condition Function
double cpu_time3= ((double)(time3-time2))
       /((double)(CLOCKS_PER_SEC));

printf(" CPU time for Hessenberg: %11.5e\n", cpu_time1);
printf(" CPU time for QR: %11.5e\n", cpu_time2);
printf(" CPU time for Conditional Number: %11.5e\n", cpu_time3);



//Creates txt file
char filename[]= "cputime.txt";
FILE* outfile= fopen(filename,"a+");
// FILE* outfile2= fopen(filename,"w+");
char x1[]= "N";
char x2[]= "cputime Hessenberg";
char x3[]= "cputime QRA";
char x4[]= "cputime Condition Number";
char x5[]= "Condition Number";
// fprintf(outfile,"%15s %15s %15s %15s %15s\n",x1,x2,x3,x4,x5);
fprintf(outfile,"%15i %15f %15f %15f %15f\n",N,cpu_time1,cpu_time2,cpu_time3,conditionnumber);
int fclose(FILE* filename);


delete_matrix(&A);
delete_trimatrix(&T);
}
