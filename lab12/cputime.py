def cpu_plot(infile):
    import numpy as np
    import matplotlib.pyplot as plt
    x= [];
    y= [];

    with open(infile) as f:
        for line in f:
            item1,item2= map(float,line.split())
            x.append(item1)
            y.append(item2)
    N= np.array(x)
    cpu= np.array(y)

    plt.plot(N,cpu)
    plt.title('Matrix Size vs CPU Time')
    plt.xlabel("Matrix Size N")
    plt.ylabel("Computational Time (Seconds)")
    plt.savefig('CPUPlot.png')
    plt.show()

    return N,cpu

if __name__=="__main__":
    data= 'cputime.data'
    plot= cpu_plot(data)
