#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include "matrix.h"

matrix new_matrix(const int rows, const int cols)
{
  matrix mat;
  mat.rows = rows;
  mat.cols = cols;
  assert(rows>0);
  assert(cols>0);
  mat.val = (double*)malloc(sizeof(double)*rows*cols);

  for (int i=0; i<(rows*cols); i++)
    {  mat.val[i] = 0.0;  }

  return mat;
}

void print_matrix_full(const matrix* mat, char* varname)
{
  assert(mat->rows>0); assert(mat->cols>0);
  printf("\n %.100s =\n", &varname[1] );
  for(int i=1; i<=mat->rows; i++ )
    {
      printf("  |  ");
      for(int j=1; j<=mat->cols; j++)
        {
          printf("%10.3e",mgetp(mat,i,j));
          if (j<mat->cols) {printf(", ");}
          else {printf(" ");}
        }
      printf("|\n");
    }
  printf("\n");
}

// matrix tridiagonal (const vector* tri,int N)
// {
//   for (int i = 0; i< N; i++)
//   {
//     for (int j=0; j<N; j++)
//     {
//
//     }
//   }
//
//   return C;
// }

matrix matrix_add(const matrix* A, const matrix* B)
{
  const int rows = A->rows;
  const int cols = A->cols;
  assert(rows==B->rows);
  assert(cols==B->cols);
  matrix C = new_matrix(rows,cols);

  for (int i=1; i<=rows; i++)
    for (int j=1; j<=cols; j++)
      {
        mget(C,i,j) = mgetp(A,i,j)+mgetp(B,i,j);
      }

  return C;
}

matrix matrix_sub(const matrix* A, const matrix* B)
{
  const int rows = A->rows;
  const int cols = A->cols;
  assert(rows==B->rows);
  assert(cols==B->cols);
  matrix C = new_matrix(rows,cols);

  for (int i=1; i<=rows; i++)
    for (int j=1; j<=cols; j++)
      {
        mget(C,i,j) = mgetp(A,i,j)-mgetp(B,i,j);
      }

  return C;
}

matrix matrix_mult(const matrix* A, const matrix* B)
{
  const int rowsA = A->rows; const int colsA = A->cols;
  const int rowsB = B->rows; const int colsB = B->cols;
  assert(colsA==rowsB);
  matrix C = new_matrix(rowsA,colsB);

  for (int i=1; i<=rowsA; i++)
    for (int j=1; j<=colsB; j++)
      for (int k=1; k<=colsA; k++)
        {
          mget(C,i,j) += mgetp(A,i,k)*mgetp(B,k,j);
        }

  return C;
}

matrix matrix_dot_mult(const matrix* A, const matrix* B)
{
  const int rows = A->rows;
  const int cols = A->cols;
  assert(rows==B->rows);
  assert(cols==B->cols);
  matrix C = new_matrix(rows,cols);

  for (int i=1; i<=rows; i++)
    for (int j=1; j<=cols; j++)
      {
        mget(C,i,j) = mgetp(A,i,j)*mgetp(B,i,j);
      }

  return C;
}

void delete_matrix(matrix* mat)
{
  free(mat->val);
}

vector new_vector(const int size)
{
  vector vec;
  vec.size = size;
  assert(size>0);
  vec.val = (double*)malloc(sizeof(double)*size);

  for (int i=0; i<(size); i++)
    {  vec.val[i] = 0.0;  }

  return vec;
}

void print_vector_full(const vector* vec, char* varname)
{
  assert(vec->size>0);
  printf("\n");
  printf(" %.100s =\n", &varname[1] );
  printf("  |  ");
  for(int i=1; i<=vec->size; i++ )
    {
      printf("%10.3e",vgetp(vec,i));
      if (i<vec->size) {printf(", ");}
    }
  printf(" |^T\n\n");
}

vector vector_add(const vector* x, const vector* y)
{
  const int size = x->size;
  assert(size==y->size);
  vector z = new_vector(size);

  for (int i=1; i<=size; i++)
    {
      vget(z,i) = vgetp(x,i)+vgetp(y,i);
    }

  return z;
}

vector vector_sub(const vector* x, const vector* y)
{
  const int size = x->size;
  assert(size==y->size);
  vector z = new_vector(size);

  for (int i=1; i<=size; i++)
    {
      vget(z,i) = vgetp(x,i)-vgetp(y,i);
    }

  return z;
}

double vector_dot_mult(const vector* x, const vector* y)
{
  const int size = x->size; assert(size==y->size);

  double z = 0.0;
  for (int i=1; i<=size; i++)
    { z += vgetp(x,i)*vgetp(y,i); }

  return z;
}

void delete_vector(vector* vec)
{
  free(vec->val);
}

void print_scalar_full(const double* z, char* varname)
{
  printf("\n %.100s =\n", &varname[1] );
  printf("    %10.3e \n\n",*z);
}

vector matrix_vector_mult(const matrix* A, const vector* x)
{
  const int rows = A->rows; const int cols = A->cols;
  const int size = x->size;
  assert(cols==size);
  vector Ax = new_vector(rows);

  for (int i=1; i<=rows; i++)
    {
      double tmp = 0.0;
      for (int j=1; j<=size; j++)
        { tmp += mgetp(A,i,j)*vgetp(x,j); }
      vget(Ax,i) = tmp;
    }

  return Ax;
}

// Function that calculates V/||v||
vector normalize(const vector* v)
{
  const int size = v->size;
  double sum = 0;
  for (int i = 0; i <= size; i++)
  {
     sum = pow(v->val[i],2) + sum;

  }
  double mag = sqrt(sum);
  const vector vnorm = new_vector(size); //creates normal
  // for (int i=0; i<= size;i++)
  // {
  //   vget(vnorm,i)= v->val[i]/mag;
  // }
  vget(vnorm,1) = v->val[0] / mag;
  vget(vnorm,2) = v->val[1] / mag;
  vget(vnorm,3)= v->val[2] / mag;

  return vnorm;
}

vector PowIter(const vector* v,double TOL, int MaxIters, const matrix* A, int N)
{
  vector v0 = normalize(v); //normalizes guess vector
  // print_vector(&v0);
  vector w= matrix_vector_mult(A,&v0); //calculates Av(0)
  double lambdabig = vector_dot_mult(&v0,&w);//calculates guess Lambda
  int mstop = 0;
  int k = 0;
  int loopcount= 0;
  while (mstop == 0)
  { k++;
    vector vk = normalize(&w);
    vector avk = matrix_vector_mult(A,&vk);
    double lambdanew = vector_dot_mult(&vk,&avk);
    if (fabs(lambdanew - lambdabig) < TOL || k==MaxIters)
    {
      mstop =1;
    }
    // vget(w,1) = vget(avk,1);
    // vget(w,2)= vget(avk,2);
    // vget(w,3)= vget(avk,3);
    for(int i=1; i<=N; i++ )
    {
      vget(w,i)=vget(avk,i);
    }
    // print_vector(&w);
    lambdabig = lambdanew;
    loopcount+= 1;

  }
  printf("It took %d iterations to for Power Iteration Method converge to %f\n",loopcount,lambdabig);
  return w;
}

vector InversIter (const vector* v,double TOL, int MaxIters,const matrix* A, int N)
{
  //calculates initial values
  vector v0 = normalize(v); //sets initial guess vector v(0)
  // print_vector(&v0);
  vector av0= matrix_vector_mult(A,&v0); //calculates Av(0)
  double lambdasmall = vector_dot_mult(&v0,&av0);//calculates guess Lambda
  //Creates Identity Matrix * mu
  int size = A->rows;
  double mu = 0.05;
  matrix ul = new_matrix(size,size);
  for(int i=1; i<=N; i++ )
    for (int j=1; j<=N; j++ )
      {
        mget(ul,i,j) = mu*(i==j);
      }
  // print_matrix(&ul);
  //calcuates (A-uI)
  matrix A2 = matrix_sub(A,&ul);
  // print_matrix(&A2);
  vector w = solve(&A2,&v0);
  //Shifted Power Iteration Algorithm
  int mstop = 0;
  int k =0;
  int loopcount = 0;
  while (mstop == 0)
  { k++;
    vector vk = normalize(&w);
    vector avk = matrix_vector_mult(A,&vk);
    double lambdanew = vector_dot_mult(&vk,&avk);
    if (fabs(lambdanew - lambdasmall) < TOL || k==MaxIters)
    {
      mstop =1;
    }
    w = solve(&A2,&vk);
    lambdasmall = lambdanew;
    loopcount+= 1;

  }
  printf("It took %d iterations for Shifted Power Iteration Method to converge to %f\n",loopcount,lambdasmall);
  return w;
}
//
double RQiter(const vector* v,double TOL, int MaxIters,const matrix* A, int N)
{
  int size = A->rows;
  vector v0 = normalize(v);

  vector av0 = matrix_vector_mult(A,&v0);
  double lambda = vector_dot_mult(&v0,&av0);
  double mu = 0.5;
  mu = 0.5+(1/lambda);
  matrix ul = new_matrix(size,size);
  for(int i=1; i<=N; i++ )
    for (int j=1; j<=N; j++ )
      {
        mget(ul,i,j) = lambda*(i==j);
      }
  matrix A2 = matrix_sub(A,&ul);
  // print_matrix(&ul);
  vector w = solve(&A2,&v0);
  int mstop = 0;
  int k = 0;
  int loopcount = 0;
  while (mstop == 0)
  {
    k++;
      vector vk = normalize(&w);
      vector avk = matrix_vector_mult(A,&vk);
      double lambdanew = vector_dot_mult(&vk,&avk);

      if (fabs(lambdanew - lambda) < TOL || k==MaxIters)
      {
        mstop =1;
      }
      matrix ulnew = new_matrix(size,size);
      for(int i=1; i<=N; i++ )
        for (int j=1; j<=N; j++ )
          {
            mget(ul,i,j) = lambdanew*(i==j);
          }
      matrix Anew = matrix_sub(A,&ul);
      w = solve(&Anew,&vk);
      lambda = lambdanew;
      loopcount += 1;
  }

  printf("It took %d iterations for Rayleigh Quotient Iteration Method to converge to %f\n",loopcount,lambda);
  return lambda;
}

vector solve(const matrix* A, const vector* b)
{
  const int rows = A->rows; const int cols = A->cols;
  const int size = b->size;
  assert(rows==cols); assert(rows==size);

  vector x = new_vector(rows);

  for (int i=1; i<=(size-1); i++) // LOOP OVER EACH COLUMN
    {
      // Select largest pivot in current column
      int p=i; double maxA = -100.0e0;
      for (int j=i; j<=size; j++)
        {
          double tmp = fabs(mgetp(A,j,i));
          if ( tmp > maxA ){ p = j; maxA = tmp; }
        }

      // See if matrix is singular
      if (maxA <= 1.0e-14)
        { printf(" Cannot invert system\n"); exit(1); }

      // Pivot (aka interchange rows)
      if (p!=i)
        {
          for (int j=1; j<=size; j++)
            {
              double tmp1  = mgetp(A,i,j);
              mgetp(A,i,j) = mgetp(A,p,j);
              mgetp(A,p,j) = tmp1;
            }

          double tmp2 = vgetp(b,i);
          vgetp(b,i) = vgetp(b,p);
          vgetp(b,p) = tmp2;
        }

      // Eliminate below diagonal
      for (int j=(i+1); j<=size; j++)
        {
          double dm = mgetp(A,j,i)/mgetp(A,i,i);
          for (int k=1; k<=size; k++)
            { mgetp(A,j,k) = mgetp(A,j,k) - dm*mgetp(A,i,k); }
          vgetp(b,j) = vgetp(b,j) - dm*vgetp(b,i);
        }
    }

  // Backward substitution
  vget(x,size) = vgetp(b,size)/mgetp(A,size,size);
  for (int j=1; j<=(size-1); j++)
    {
      double sum = 0.0e0;

      for (int k=(size-j+1); k<=size; k++)
        { sum = sum + mgetp(A,size-j,k)*vget(x,k); }

      vget(x,size-j) = (vgetp(b,size-j) - sum)
                     /mgetp(A,size-j,size-j);
    }

  return x;
}
