#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "matrix.h"
#include <time.h>


int main()
{
  //Matrices
  matrix A = new_matrix(3,3);
  matrix B = new_matrix(3,3);

  for(int i=1; i<=3; i++ )
    for (int j=1; j<=3; j++ )
      {
        mget(A,i,j) = i*(i==j)+ 1*(i==1)
                       + 1*(i==2) + 1* (i==3);
        mget(B,i,j) =  2.0*(i==j)
                    +  1.0*(i-1==j) + 1.0*(j-1==i);
      }
  int N;
  printf("Enter a value for size N: ");
  scanf("%d",&N);
  vector tri = new_vector(3);
  vget(tri,0)= -1.0; vget(tri,1)= 2; vget(tri,2) = -1.0;
  matrix C = new_matrix(N,N);
  vector v2 = new_vector(N);
  for(int i=1; i<=N; i++ )
  {
    vget(v2,i)= 1.0;
    for (int j=1; j<=N; j++ )
      {
        mget(C,i,j) =   2.0*(i==j)
                    +  -1.0*(i-1==j) + -1.0*(j-1==i);
      }
  }
  vget(v2,N)=0;
  // vget(v2,N-1)=0;
  print_vector(&v2);
  print_matrix(&C);
  print_vector(&v2);

  // Print matrices
  print_matrix(&A);
  print_matrix(&B);

  // Add/Subtract/Multiply matrices
  matrix  Csum = matrix_add(&A,&B); print_matrix(&Csum);
  matrix Cdiff = matrix_sub(&A,&B); print_matrix(&Cdiff);
  matrix Cprod = matrix_mult(&A,&B); print_matrix(&Cprod);
  matrix  Cdot = matrix_dot_mult(&A,&B); print_matrix(&Cdot);

  // Vectors
  vector x = new_vector(3);
  vector y = new_vector(3);
  vector v = new_vector(3);

  vget(v,1)=1.0;
  vget(v,2)=1.0;
  vget(v,3)=1.0;


  // Eigen Setup
  double TOL =  pow(10,-8);
  printf("%g\n",TOL);
  int MaxIter = 500;
  // Print vectors

  print_vector(&v);

  //Eigen Values power iter
  vector eigenbig= PowIter(&v,TOL,MaxIter,&A,3);
  // printf("%f\n",eigenbig);
  //Eigen Values inverse power iterations
   // double eigensmall = InversIter(&v,TOL,MaxIter,&A,3);
   vector eigenguess = InversIter(&v,TOL,MaxIter,&A,3);
   // print_vector(&eigenguess);
  //Eigen Values Rayleigh Quotient
   double eigen = RQiter(&v,TOL,MaxIter,&A,3);

   // clock times
   clock_t time0,time1;
    time0=clock();
    vector small =InversIter(&v2,TOL,10,&C,N);
    vector large = PowIter(&v2,TOL,5,&C,N);
   double eigen1 = RQiter(&small,TOL,MaxIter,&C,N); //gets smallest value
   double eignen2 = RQiter(&large,TOL,MaxIter,&C,N); //gets largest value
   time1=clock();

    //Report Times
    double cpu_time1 = (( double )(time1 - time0 ))/((double)( CLOCKS_PER_SEC));
    printf (" cpu_time1 = %e\n",cpu_time1 );

    //Creates data file
    char filename[]= "cputime.data";
    FILE* outfile= fopen(filename,"a+");
    fprintf(outfile,"%15i %15f\n",N,cpu_time1);
    int fclose(FILE* filename);


//vectors

    vget(x,1) = 1.0;  vget(y,1) = 1.0;
    vget(x,2) = 0.0;  vget(y,2) = 2.0;
    vget(x,3) = 1.0;  vget(y,3) = 3.0;
    // vget(x,4) = 0.0;  vget(y,4) = 4.0;
    // vget(x,5) = 1.0;  vget(y,5) = 5.0;
    // print_vector(&x);
    // print_vector(&y);


  // Add/Subtract/Multiply vectors
  vector  zsum = vector_add(&x,&y); //print_vector(&zsum);
  vector zdiff = vector_sub(&x,&y);//print_vector(&zdiff);
  double  zdot = vector_dot_mult(&x,&y); //print_scalar(&zdot);

  // Matrix vector multiply
  vector Ax = matrix_vector_mult(&A,&x);
  // print_vector(&Ax);

  vector Ay= matrix_vector_mult(&A,&y);
  // print_vector(&Ay);

  // Linear solve via Gaussian elimination
  vector soln = solve(&A,&y);
  // print_vector(&soln);


  // Cleanup
  delete_matrix(&A);     delete_matrix(&B);
  delete_matrix(&Csum);  delete_matrix(&Cdiff);
  delete_matrix(&Cprod); delete_matrix(&Cdot);
  delete_vector(&x);     delete_vector(&y);
  delete_vector(&zsum);  delete_vector(&zdiff);
  delete_vector(&Ax);    delete_vector(&soln);

}
