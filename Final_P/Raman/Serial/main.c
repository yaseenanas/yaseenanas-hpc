#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

int main (int argc, char* argv[])
{ //Initialize functions

  int N = strtol(argv[1], NULL, 10);
  //initial guess
  clock_t time0, time1;
  time0= clock();
  long double Raman(const int N);
  long double pie = 1/Raman(N);
  time1= clock();
  long double cpu_time= ((double)(time1-time0))
      /((double)(CLOCKS_PER_SEC));
      printf("CPU time for Jacobi Algorithm is: %11.5Le\n", cpu_time);
      long double pieact = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679;
      long double error = fabs((pieact - pie)/pieact);
      printf("The error is %Lg\n", error);

    char filename[]= "Raman.txt";
    FILE* outfile= fopen(filename, "a+");
    fprintf(outfile,"%15i %15Lf %15Lg\n",N,cpu_time,error);
    int fclose(FILE* filename);

  return 0;
}
//Function to calculate pi
long double Raman(const int N)
{  double factorial(const int N);
   long double piestimate =0;
   long double c = (2*sqrt(2))/9801;
   long double k;
   for (int i =0; i<=N; i++)
   { k = factorial(i);
     piestimate+= (factorial(4*i)*(1103+26390*i))/(pow(k,4)*pow(396,4*i));
   }

   return c*piestimate;
}

double factorial(const int N)
{ double fact =1;
  for (int i=1; i<=N; i++)
  {
    fact*= i;
  }
  return fact;
}

void usage(const char *prog_name)
{
  fprintf(stderr,"usage: %s <N>\n",prog_name);
  fprintf(stderr,"   N should be positive\n");
  exit(1);
}
