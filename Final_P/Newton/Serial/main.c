#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

int main (int argc, char* argv[])
{ //Initialize functions
  long double Newton(const int N);
  double factorial(const int N);

  int N = strtol(argv[1], NULL, 10);
  //initial guess
  clock_t time0, time1;
    time0= clock();
    long double sum = Newton(N);
    time1= clock();
    long double cpu_time= ((double)(time1-time0))
        /((double)(CLOCKS_PER_SEC));
    printf("CPU time for Jacobi Algorithm is: %11.5Le\n", cpu_time);
    long double pie = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679;
    long double error = fabs((pie - sum)/pie);
    printf("The error is %Lg\n", error);
     printf("%Lf\n", sum);

  //Writes data into txt file
char filename[]= "Newton.txt";
FILE* outfile= fopen(filename, "a+");
 fprintf(outfile,"%15i %15Lf %15Lg\n",N,cpu_time,error);
int fclose(FILE* filename);

  return 0;
}
//Function to calculate pi
long double Newton(const int N)
{ double factorial(const int N);
  long double piestimate =0;
   double kfact;
   for (int i =0; i<=N; i++)
   { kfact= factorial(i);
     piestimate+= pow(2,i)*kfact*kfact/(factorial(2*i+1));
   }

   return 2*piestimate;
}

double factorial(const int N)
{ double fact =1;
  for (int i=1; i<=N; i++)
  {
    fact*= i;
  }
  return fact;
}


void usage(const char *prog_name)
{
  fprintf(stderr,"usage: %s <N>\n",prog_name);
  fprintf(stderr,"   N should be positive\n");
  exit(1);
}
