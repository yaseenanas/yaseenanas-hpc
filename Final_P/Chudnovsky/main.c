#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

int main (int argc, char* argv[])
{ //Initialize functions

  int N = strtol(argv[1], NULL, 10);
  long double Chud(const int N);
  //initial guess
  clock_t time0, time1;
  time0= clock();
  long double pie= Chud(N);
  time1= clock();
  long double cpu_time= ((double)(time1-time0))
      /((double)(CLOCKS_PER_SEC));
      printf("CPU time for Jacobi Algorithm is: %11.5Le\n", cpu_time);
      long double pieact = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679;
      long double error = fabs((pieact - pie)/pieact);
      printf("The error is %Lg\n", error);

    char filename[]= "Chud.txt";
    FILE* outfile= fopen(filename, "a+");
    fprintf(outfile,"%15i %15Lf %15Lg\n",N,cpu_time,error);
    int fclose(FILE* filename);

  return 0;
}
//Function to calculate pi
long double Chud(const int N)
{  double factorial(const int N);
   long double piestimate;
   long double s =0;
   const long double k1 = 545140134, k2 = 13591409, k3 = -640320;
   const long double k4 = 426880, k5 = 10005;
   for (int i =0; i<=N; i++)
   {
     s+= (factorial(6*i) * (k1*i + k2))/(factorial(3*i) * pow(factorial(i),3) * pow(k3, 3*i));
   }
    piestimate = (k4*sqrt(k5))/s;

   return piestimate;
}

double factorial(const int N)
{ double fact =1;
  for (int i=1; i<=N; i++)
  {
    fact= i*fact;
  }
  return fact;
}

void usage(const char *prog_name)
{
  fprintf(stderr,"usage: %s <N>\n",prog_name);
  fprintf(stderr,"   N should be positive\n");
  exit(1);
}
