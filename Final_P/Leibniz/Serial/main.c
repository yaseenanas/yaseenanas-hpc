#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

int main (int argc, char* argv[])
{ //Initialize functions

  int N = strtol(argv[1], NULL, 10);
  //initial guess
  clock_t time0, time1;

  long double Leibniz(const int N);
  time0= clock();
  long double sum = Leibniz(N);
  time1= clock();
  long double cpu_time= ((double)(time1-time0))
      /((double)(CLOCKS_PER_SEC));
  printf("CPU time for Jacobi Algorithm is: %11.5Le\n", cpu_time);
  printf("%Lg\n", sum);
  long double pie = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679;
  long double error = fabs((pie - sum)/pie);
  printf("The error is %Lg\n", error);

//Writes data into txt file
 char filename[]= "Leibniz.txt";
 FILE* outfile= fopen(filename, "a+");
 fprintf(outfile,"%15i %15Lf %15Lg\n",N,cpu_time,error);
 int fclose(FILE* filename);

  return 0;
}
//Function to calculate pi
long double Leibniz(const int N)
{  //Initialize piestimate
   long double sum = pow(-1,0)/(2*0+1);
   for (int i =1; i<=N; i++)
   {
     sum+= pow(-1,i)/(2*i+1);
   }

   return 4*sum;
}


void usage(const char *prog_name)
{
  fprintf(stderr,"usage: %s <N>\n",prog_name);
  fprintf(stderr,"   N should be positive\n");
  exit(1);
}
