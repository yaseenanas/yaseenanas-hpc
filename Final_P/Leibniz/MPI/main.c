#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <mpi.h>

int main (int argc, char* argv[])
{
  //Initialize functions and gets inputs
  double Leibniz(const int N, const int min);
  void get_input(int argc, char* argv[],
  	       const int my_rank,
  	       const int comm_sz,
  	       int* N);
//Setup MPI Code
  int comm_sz, my_rank;
  MPI_Init(NULL,NULL);

  //Total Number of process
  MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);

  //get id of process
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  //Grab the global parameters
  double global_pie;
  int N;
  get_input(argc,argv,my_rank,comm_sz,&N);

  //Get time
  double time_start;
  if (my_rank==0)
    { time_start = MPI_Wtime(); }

  //Run local Leibniz
  const int start= (N*my_rank)/comm_sz +1;
  // printf("The start is: %d\n", start);
  const int final = N*(my_rank+1)/comm_sz;
    // printf("The start is: %d\n", final);
  double local_pie = Leibniz(final,start);
  // printf("I am the Process %d and my local pie is %f \n", my_rank, local_pie);

  //the slaves sending back the local pie estimates
  if(my_rank !=0)
  {
    MPI_Send(&local_pie,1,MPI_DOUBLE,0,0,
              MPI_COMM_WORLD);
  }
  else //the master recieves the local pie estimates
  {
    global_pie = local_pie;
    // printf("\n\nI am master and my sum currently is : %f\n", global_pie);
    for (int i=1; i<comm_sz; i++)
        {
          MPI_Recv(&local_pie,1,MPI_DOUBLE,i,0,
                   MPI_COMM_WORLD,MPI_STATUS_IGNORE);
                   global_pie+=local_pie;
          // printf("The sum yet is %f after adding sum %f from process %d\n", global_pie, local_pie, i);
        }
  }

  //

  //Print Answer to screen
  if (my_rank==0)
   {
     double time_end = MPI_Wtime();
     double time_elapsed = time_end - time_start;
     printf(" NP = %2i, N = %i, pie = %20.13e",comm_sz,N,4*global_pie);
     long double pieact = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679;
     long double error = fabs((pieact - (4*global_pie))/pieact);
     printf("     Elapsed time = %20.13e\n",time_elapsed);
     printf(" Error is %Lg\n", error);

     char filename[]= "openmpi.txt";
     FILE* outfile= fopen(filename, "a+");
     fprintf(outfile,"%15i %15i %15f %15Lg\n",N,comm_sz,time_elapsed,error);
     int fclose(FILE* filename);
   }
//End Program
MPI_Finalize();
  return 0;
}
//Function to calculate pi
double Leibniz(const int N, const int min)
{  long double sum = pow(-1,(min-1))/(2*(min-1)+1);
   for (int i =min; i<=N; i++)
   {
     sum+= pow(-1,i)/(2*i+1);
   }


   return sum;
}


void get_input(int argc, char* argv[],
	       const int my_rank,
	       const int comm_sz,
	       int* N)
{
  void usage(const char *prog_name);
  if (my_rank==0)
    {
      if (argc != 2) { usage(argv[0]); }

      *N = strtol(argv[1], NULL, 10);
      if (*N<=0) { usage(argv[0]); }
      if (*N%comm_sz!=0) { usage(argv[0]); }

      for (int i=1; i<comm_sz; i++)
        {
          MPI_Send(N,1,MPI_INT,i,0,MPI_COMM_WORLD);
        }
    }
  else
    {
      MPI_Recv(N,1,MPI_INT,0,0,MPI_COMM_WORLD,
               MPI_STATUS_IGNORE);
    }
}

void usage(const char *prog_name)
{
  fprintf(stderr,"usage: %s <N>\n",prog_name);
  fprintf(stderr,"   N should be positive\n");
  fprintf(stderr,"   N should be exactly divisible "
                 "by the number of processors\n");
  exit(1);
}
