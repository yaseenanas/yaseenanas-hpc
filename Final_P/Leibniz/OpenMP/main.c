#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#ifdef _OPENMP
#  include <omp.h>
#endif

int main (int argc, char* argv[])
{ //Initialize functions
  long double Leibniz(const int N, const int thead_count);
  int thread_count = strtol(argv[1], NULL, 10);
  int N = strtol(argv[2], NULL, 10);
  const double time1 = omp_get_wtime();
  long double pie = Leibniz(N, thread_count);
  const double time2 = omp_get_wtime();
  const double cpu_time= time2 -time1;
  long double pieact = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679;
  const long double error = fabs((pieact - pie)/pieact);
  printf("With %2i threads, clock_time = %11.5e (sec), N = %i, error = %11.5Lg\n\n", thread_count, cpu_time, N ,error);

  //Creates txt file
  char filename[]= "openmp.txt";
  FILE* outfile= fopen(filename, "a+");
  fprintf(outfile,"%15i %15i %15f %15Lg\n",N,thread_count,cpu_time,error);
  int fclose(FILE* filename);

  return 0;
}
//Function to calculate pi
long double Leibniz(const int N, const int thread_count)
{  long double piestimate =0;
   // srand(time(NULL));
  # pragma omp parallel for num_threads(thread_count) reduction(+:piestimate)
  for (int i =0; i<=N; i++)
  {
    piestimate+= pow(-1,i)/(2*i+1);
  }

   return 4*piestimate;
}


void usage(const char *prog_name)
{
  fprintf(stderr,"usage: %s <N>\n",prog_name);
  fprintf(stderr,"   N should be positive\n");
  exit(1);
}
