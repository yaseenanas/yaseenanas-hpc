#!/bin/bash

# Copy/paste this job script into a text file and submit with the command:
#    sbatch thefilename
# job standard output will go to the file slurm-%j.out (where %j is the job ID)

#SBATCH --time=1:00:00   # walltime limit (HH:MM:SS)
#SBATCH --nodes=2   # number of nodes
#SBATCH --ntasks-per-node=16   # 16 processor core(s) per node
#SBATCH --mail-user=asyaseen@iastate.edu   # email address
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL

# LOAD MODULES, INSERT CODE, AND RUN YOUR PROGRAMS HERE

make
./main.exe 1 32000000 >>output.txt
./main.exe 2 32000000 >>output.txt
./main.exe 4 32000000 >>output.txt
./main.exe 8 32000000 >>output.txt
./main.exe 16 32000000 >>output.txt
./main.exe 32 32000000 >>output.txt


