def bin2int(a):
	b= {'0','1'}
	if b==set(a) or set(a)== {'0'} or set(a)== {'1'}:
	# if len(a)== 16:
		a= list(a)
		a.reverse()
		a= list(map(int,a))
		M= len(a)
		y=0
		x= -a[M-1]*2**(M-1)
		for n in range (0,M-1):
			y= y+a[n]*2**(n)
		return y+x
		# else:
		# 	print(a, "Is not of size 16")
	else:
		print(a, "Is not a binary string")


def int2bin(x):
	if type(x)==int:
		bin_string = [];
		if x==0:
			for n in range(0,16):
				bin_string.append('0')
		else:
			if x<0:
				tmp = '1'
				xmod = x + 2**15
			else:
					tmp = '0'
					xmod = x
			bin_string.append(tmp)


			for n in range(1,16):
				pw = 2**(15-n)
				z = int(xmod/pw)
				xmod = xmod - z*pw
				tmp = str(z)
				bin_string.append(tmp)

		bin_string = ''.join(bin_string)
		return bin_string
	else:
		print("Sorry, ",x," is not an integer")


def test(a,b,c,d,e):
	f= int2bin(a)
	g= int2bin(b)
	h= int2bin(c)
	i= int2bin(d)
	j= int2bin(e)
	if bin2int(f)== a and bin2int(g)==b and bin2int(h)==c and bin2int(i)==d and bin2int(j)==e:
		print("The function works")
	else:
		print("The function does not work")
	return f,g,h,i,j


def add2bins(bin1,bin2): #Function that adds 2 bin strings
	num1= bin2int(bin1)
	num2= bin2int(bin2)
	sumnum= num1+num2
	binsum= int2bin(sumnum)
	return binsum



trial1=bin2int("111")
trial2= bin2int("1111111100000010")
print(trial2)

trial3= int2bin(-254.4)
trial4= int2bin(-254)
trial5= int2bin("number")

bin1= '1111111100000010'
bin2= '1111111110001001'
Binadd= add2bins(bin1,bin2)
print('The sum of the two binary is: ', Binadd)

a= 0
b= -254
c= -120
d= 254
e= 120
f= test(a,b,c,d,e)
print(f)
