#!/bin/bash

# Copy/paste this job script into a text file and submit with the command:
#    sbatch thefilename
# job standard output will go to the file slurm-%j.out (where %j is the job ID)

#SBATCH --time=1:00:00   # walltime limit (HH:MM:SS)
#SBATCH --nodes=1   # number of nodes
#SBATCH --ntasks-per-node=16   # 16 processor core(s) per node
#SBATCH --mail-user=asyaseen@iastate.edu   # email address
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL

# LOAD MODULES, INSERT CODE, AND RUN YOUR PROGRAMS HERE

module load intel
make clean
make
mpirun -np 1 ./main.exe 16000000 > output.txt
mpirun -np 2 ./main.exe 16000000 >> output.txt
mpirun -np 4 ./main.exe 16000000 >> output.txt
mpirun -np 8 ./main.exe 16000000 >> output.txt
mpirun -np 16 ./main.exe 16000000 >> output.txt
