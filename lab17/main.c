# include <stdio.h>
# include <stdlib.h>
#include <time.h>
# include <math.h>
#include <mpi.h>


int main(int argc, char* argv[])
{
  //Setup MPI CODE
 int comm_sz, my_rank;
 MPI_Init(NULL,NULL);
 MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
 MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

 //Grab the global N Parameter
 //and set the local N Parameter
int N;
void get_input(int argc, char* argv[],
               const int my_rank,
               const int comm_sz,
               int* N);
get_input(argc,argv,my_rank,comm_sz,&N);

// Get time
double time_start;
if (my_rank==0)
  { time_start = MPI_Wtime(); }

//Run local Monte Carlo Simulation
 double monte(int N);
 const int N_local= N/comm_sz;
 double local_piestimate = monte(N_local);
 double global_piestimate;
 const double pi = 3.141592653589793238;
 double error;

 //add local results to global result on processor 0

 if (my_rank != 0)
 {
   MPI_Send(&local_piestimate,1,MPI_DOUBLE,0,0,MPI_COMM_WORLD);
 }
 else
 {
   global_piestimate = local_piestimate;
      for (int i=1; i<comm_sz; i++)
        {
          MPI_Recv(&local_piestimate,1,MPI_DOUBLE,i,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
          global_piestimate += local_piestimate;
        }

          double pi_estimate = 4*(global_piestimate/N) ;
          error = fabs(pi-pi_estimate)/pi;
 }


  // Print answer to screen
  if (my_rank==0)
    {
      double time_end = MPI_Wtime();
      double time_elapsed = time_end - time_start;
      printf(" NP = %2i, N = %i, error = %20.13e",comm_sz,N,error);
      printf("     Elapsed time = %20.13e\n",time_elapsed);
    }
// End program
  MPI_Finalize();
  return 0;
}

double monte(int N)
{

  double r;
  double x;
  double y;
  int in_circle;
  double pie;
  for (int i=0; i<N; i++)
  {
    x= ((double)rand()/(double)RAND_MAX);
    y= ((double)rand()/(double)RAND_MAX);
    // xsqr= pow(x,2);
    // ysqr= pow(y,2);
    r= sqrt(x*x+y*y);
    if (r<= 1.0)
    {
     in_circle+= 1;
    }
  }

  pie= (double)in_circle/N*4;
  return pie;
  //
}


void get_input(int argc, char* argv[],
	       const int my_rank,
	       const int comm_sz,
	       int* N)
{
  void usage(const char *prog_name);
  if (my_rank==0)
    {
      if (argc != 2) { usage(argv[0]); }

      *N = strtol(argv[1], NULL, 10);
      if (*N<=0) { usage(argv[0]); }
      if (*N%comm_sz!=0) { usage(argv[0]); }

      for (int i=1; i<comm_sz; i++)
        {
          MPI_Send(N,1,MPI_INT,i,0,MPI_COMM_WORLD);
        }
    }
  else
    {
      MPI_Recv(N,1,MPI_INT,0,0,MPI_COMM_WORLD,
               MPI_STATUS_IGNORE);
    }
}

void usage(const char *prog_name)
{
  fprintf(stderr,"usage: %s <N>\n",prog_name);
  fprintf(stderr,"   N should be positive\n");
  fprintf(stderr,"   N should be exactly divisible "
                 "by the number of processors\n");
  exit(1);
}
