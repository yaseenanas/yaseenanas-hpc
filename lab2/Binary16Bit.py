def bin2int(a):
	a= list(a)
	a.reverse()
	a= list(map(int,a))
	M= len(a)
	y=0
	x= -a[M-1]*2**(M-1)
	for n in range (0,M-1):
		y= y+a[n]*2**(n)
	return y+x

def int2bin(x):
	bin_string = [];
	if x==0:
		for n in range(0,16):
			bin_string.append('0')
	else:
		if x<0:
			tmp = '1'
			xmod = x + 2**15
		else:
				tmp = '0'
				xmod = x
		bin_string.append(tmp)


		for n in range(1,16):
			pw = 2**(15-n)
			z = int(xmod/pw)
			xmod = xmod - z*pw
			tmp = str(z)
			bin_string.append(tmp)
	bin_string = ''.join(bin_string)
	return bin_string

from Binary16Bit import bin2int
x= bin2int('1111111100000010')
print(x)

from Binary16Bit import int2bin
bin_string = int2bin(-254)
print (bin_string)

'''
The int2bin converts base 10 integers to binary numbers.
It first does this by generating an empty list
Then it uses an if else statement for 0 and 1
If x== 0 then it appends a 0 for range 0,16 which is the size
of the binary that = -254 base ten integer
If x is greater than zero, then it converts it to 1
The final for loop generates the binary as well as removes
the " " in the string

'''
