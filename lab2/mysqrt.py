''' my own square root function '''

def sqrt(x,s=1.0,printshow=False):

    kmax = 10;

    for k in range(0,kmax):
        if printshow:
            print("Newton Before iteration %s, s = %20.15f" % (k,s));
        s = 0.5*(s + x/s);

    if printshow:
        print("Newton After %s iterations, s = %20.15f" % (k+1,s));

    return s;

if __name__ == "__main__":

    s = sqrt(15 ,1.0,True);

    print(" ")

def sqrt_secant (f,a=1.0,b=2.0,printshow=False):
    kmax=10
    for k in range (0,kmax):
        if printshow:
            print("Secant Before iteration %s, s = %20.15f" % (k,b));
        c= (f+b*a)/(b+a)
        b=c
        a=b
        if printshow:
            print("Secant After %s iterations, s = %20.15f" % (k+1,c));
    return c
if __name__ == "__main__":

    s = sqrt_secant(15,1.0,1.0,True)
    print(" ")

'''
Compared to newton's method,secant took 1/2
as many iterations to converge compared to
newton's method. If one where to computationally
caluclate square roots, it is betrer to use
secant method as it is faster than newtons method
'''
