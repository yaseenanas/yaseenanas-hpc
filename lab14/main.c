#include <stdio.h>
#include <stdlib.h>
# include <time.h>
#include <assert.h>
# include "matrix.h"
# include <math.h>
# ifdef _OPENMP
#   include <omp.h>
# endif

vector matrix_vector_mult(const matrix* A, const vector* x,int thread_count)
{
  const int rows = A->rows; const int cols = A->cols;
  const int size = x->size;
  assert(cols==size);
  vector Ax = new_vector(rows);
  const double time1= omp_get_wtime();
# pragma omp parallel for num_threads (thread_count)
  for (int i=1; i<=rows; i++)
    {
      double tmp = 0.0;
      for (int j=1; j<=size; j++)
        { tmp += mgetp(A,i,j)*vgetp(x,j); }
      vget(Ax,i) = tmp;
    }
   const double time2= omp_get_wtime();
   const double clock_time = time2-time1;
printf("With %2i threads, clock_time = %11.5e (sec)\n", thread_count,clock_time);
  return Ax;
}

int main(int argc, char* argv[])
{
  int N = strtol(argv[1] , NULL , 10); //Matrix Size
  int T = strtol(argv[2], NULL , 10); //Thread Size
   // N= 5;
  //  // T= 3;
  // printf("Enter a value for size N: ");
  // scanf("%d", &N);
  // printf("Enter desired thread size: ");
  // scanf("%d",&T);

  matrix A= new_matrix(N,N);
  for (int i=1; i<=N; i++)
    for (int j=1; j<=i; j++)
      {
        double tmp = ((double)rand())/((double)RAND_MAX);
        tmp = (double)((int)(10000.0*tmp))/10000.0;
        mget(A,i,j) = tmp;
      }
  print_matrix(&A);

vector x = new_vector(N);
for (int i=0;i<=N; i++)
{
  vget(x,i)= i+1;
}
print_vector(&x);

vector Ax = matrix_vector_mult(&A,&x,T);
print_vector(&Ax);

return 0;
}
