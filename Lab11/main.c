#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "matrix.h"


int main()
{
  //Matrices
  matrix A = new_matrix(3,3);
  matrix B = new_matrix(3,3);

  for(int i=1; i<=3; i++ )
    for (int j=1; j<=3; j++ )
      {
        mget(A,i,j) = i*(i==j)+ 1*(i==1)
                       + 1*(i==2) + 1* (i==3);
        mget(B,i,j) =  2.0*(i==j)
                    +  1.0*(i-1==j) + 1.0*(j-1==i);
      }

  // Print matrices
  print_matrix(&A);
  print_matrix(&B);

  // Add/Subtract/Multiply matrices
  matrix  Csum = matrix_add(&A,&B); print_matrix(&Csum);
  matrix Cdiff = matrix_sub(&A,&B); print_matrix(&Cdiff);
  matrix Cprod = matrix_mult(&A,&B); print_matrix(&Cprod);
  matrix  Cdot = matrix_dot_mult(&A,&B); print_matrix(&Cdot);

  // Vectors
  vector x = new_vector(3);
  vector y = new_vector(3);
  vector v = new_vector(3);

  vget(x,1) = 1.0;  vget(y,1) = 1.0; vget(v,1)=1.0;
  vget(x,2) = 0.0;  vget(y,2) = 2.0; vget(v,2)=1.0;
  vget(x,3) = 1.0;  vget(y,3) = 3.0; vget(v,3)=1.0;
  // vget(x,4) = 0.0;  vget(y,4) = 4.0;
  // vget(x,5) = 1.0;  vget(y,5) = 5.0;

  // Eigen Setup
  double TOL =  pow(10,-8);
  printf("%g\n",TOL);
  int MaxIter = 500;
  // Print vectors
  print_vector(&x);
  print_vector(&y);
  print_vector(&v);

  //Eigen Values
  double eigen= PowIter(&v,TOL,MaxIter,&A);
  printf("%f\n",eigen);

  // Add/Subtract/Multiply vectors
  vector  zsum = vector_add(&x,&y); print_vector(&zsum);
  vector zdiff = vector_sub(&x,&y); print_vector(&zdiff);
  double  zdot = vector_dot_mult(&x,&y); print_scalar(&zdot);

  // Matrix vector multiply
  vector Ax = matrix_vector_mult(&A,&x);
  print_vector(&Ax);

  vector Ay= matrix_vector_mult(&A,&y);
  print_vector(&Ay);

  // Linear solve via Gaussian elimination
  vector soln = solve(&A,&y);
  print_vector(&soln);


  // Cleanup
  delete_matrix(&A);     delete_matrix(&B);
  delete_matrix(&Csum);  delete_matrix(&Cdiff);
  delete_matrix(&Cprod); delete_matrix(&Cdot);
  delete_vector(&x);     delete_vector(&y);
  delete_vector(&zsum);  delete_vector(&zdiff);
  delete_vector(&Ax);    delete_vector(&soln);
}
