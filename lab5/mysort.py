
def quicksort(A,lo=0,hi=-100):
    if hi==-100:
        hi=len(A)-1

    def partition(A,lo,hi):
        pivot= A[hi]
        print(pivot)
        i=lo-1
        for j in range(lo,hi):
            if A[j]<= pivot:
                i=i+1
                A[i],A[j]=A[j],A[i]
        A[i+1],A[hi]=A[hi],A[i+1]
        return i+1

    if lo<hi:
        p=partition(A,lo,hi)
        quicksort(A,lo,p-1)
        quicksort(A,p+1,hi)
    return A

def filesort(filepath):
    import read_input_file as read
    import numpy as np
    data=open(filepath,"r")
    A1= read.read_input_file()
    A2= np.array(A1)
    Asort=quicksort(A2)
    flatarray= Asort.flatten()
    print(flatarray)
    f=open("sorted.data","w")
    for x in flatarray:
        f.write(str(x) + "\n")
    f.close()
    return Asort

d=filesort("input.data")
f=open("sorted.data","r")
# print(f.read())
