#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct fraction fraction;
struct fraction
{
   int numer; int denom;
};
typedef struct properfraction properfraction;
struct properfraction
{
  int whole;
  int numer;
  int denom;
};

int main()
{
   fraction a,b,sum,diff,prod,quot;
   properfraction sum_red,diff_red,prod_red,quot_red;

   int x;
   int y;
   int i;
   int j;
   // printf("Enter numerator of first fraction: ");
   // scanf("%d",&x);
   // printf("Enter denominator of first fraction: ");
   // scanf("%d",&y);
   // printf("Enter numerator of second fraction: ");
   // scanf("%d",&i);
   // printf("Enter denominator of second fraction: ");
   // scanf("%d",&j);
   // a.numer = x; a.denom = y;
   // b.numer = i; b.denom = j;

   a.numer = 1; a.denom = 10;
   b.numer = 5; b.denom = 3;

   void fraction_add(const fraction* a,
                     const fraction* b,
                     fraction* sum);
   fraction_add(&a,&b,&sum);



  void fraction_subtract(const fraction* a,
                       const fraction* b,
                       fraction* diff);
      fraction_subtract(&a,&b,&diff);


  void fraction_mult(const fraction* a,
                     const fraction* b,
                     fraction* prod);
        fraction_mult(&a,&b,&prod);


  void fraction_divide(const fraction* a,
                       const fraction* b,
                      fraction* quot);
        fraction_divide(&a,&b,&quot);

  void make_proper(const fraction* in, properfraction* out);
  make_proper(&sum,&sum_red);
  if (abs(sum_red.whole)>0)
  {
    printf("\n %i/%i  +  %i/%i  =  %i(%i/%i)\n\n",
           a.numer,a.denom,b.numer,b.denom,
           sum_red.whole,sum_red.numer,sum_red.denom);
  }

  else
  {
    printf("\n %i/%i  +  %i/%i  =  %i/%i\n\n",
           a.numer,a.denom,b.numer,b.denom,
           sum.numer,sum.denom);
  }

  void make_proper(const fraction* in, properfraction* out);
  make_proper(&diff,&diff_red);
  if (abs(diff_red.whole)>0)
  {
    printf("\n %i/%i  -  %i/%i  =  %i(%i/%i)\n\n",
           a.numer,a.denom,b.numer,b.denom,
           diff_red.whole,diff_red.numer,diff_red.denom);
  }

  else
  {
    printf("\n %i/%i  -  %i/%i  =  %i/%i\n\n",
           a.numer,a.denom,b.numer,b.denom,
           diff.numer,diff.denom);
  }

  void make_proper(const fraction* in, properfraction* out);
  make_proper(&prod,&prod_red);
  if (abs(prod_red.whole)>0)
  {
    printf("\n %i/%i  *  %i/%i  =  %i(%i/%i)\n\n",
           a.numer,a.denom,b.numer,b.denom,
           prod_red.whole,prod_red.numer,prod_red.denom);
  }

  else
  {
    printf("\n %i/%i  *  %i/%i  =  %i/%i\n\n",
           a.numer,a.denom,b.numer,b.denom,
           prod.numer,prod.denom);
  }

  void make_proper(const fraction* in, properfraction* out);
  make_proper(&quot,&quot_red);
  if (abs(quot_red.whole)>0)
  {
    printf("\n %i/%i  /  %i/%i  =  %i(%i/%i)\n\n",
           a.numer,a.denom,b.numer,b.denom,
           quot_red.whole,quot_red.numer,quot_red.denom);
  }

  else
  {
    printf("\n %i/%i  /  %i/%i  =  %i/%i\n\n",
           a.numer,a.denom,b.numer,b.denom,
           quot.numer,quot.denom);
  }

}
void make_proper(const fraction* in,
                 properfraction* out)
   {
     int n =1;
     if (in->numer <0 || in->denom <0)
     {
       n= -1;
     }

     if (abs(in->numer) < abs(in->denom))
     {

       out->whole= (in->numer)/(in->denom);

     }

     else if (abs(in->numer) > abs(in->denom))
     {
       out->whole= in->numer/in->denom;
       out->numer=(in->numer % in->denom)*n;
       out->denom= (in->denom);
     }
   }
void fraction_subtract(const fraction*a,
                      const fraction*b,
                    fraction* diff)
            {
              diff->denom= a->denom * b->denom;
              diff->numer= a->numer * b->denom - b->numer * a->denom;
              void fraction_reduce(fraction* diff);
              fraction_reduce(diff);
            }
void fraction_add(const fraction* a,
                  const fraction* b,
                  fraction* sum)
{
   sum->denom = a->denom * b->denom;
   sum->numer = a->numer * b->denom + b->numer * a->denom;

   void fraction_reduce(fraction* sum);
   fraction_reduce(sum);
}
void fraction_mult(const fraction* a,
                  const fraction* b,
                  fraction* prod)
        {
          prod->denom = a->denom * b->denom;
          prod->numer= a->numer * b->numer;
          void fraction_reduce(fraction* prod);
          fraction_reduce(prod);
        }
void fraction_divide(const fraction* a,
                     const fraction* b,
                     fraction* quot)
        {
          quot-> numer = a->numer * b->denom;
          quot ->denom= a->denom * b->numer;
          void fraction_reduce(fraction* quot);
          fraction_reduce(quot);
        }
void fraction_reduce(fraction* sum)
{
   void get_prime_factors(int n,
                          int prime_list[],
                          int* num_primes);

   if ( (sum->numer < 0) && (sum->denom < 0) )
   { sum->numer = abs(sum->numer);
     sum->denom = abs(sum->denom); }

   int prime1[100]; int num_prime_1;
   int msign1 = 1; if (sum->numer < 0 || sum->denom <0) { msign1 = -1; }
   sum->numer = abs(sum->numer);
   get_prime_factors(sum->numer,prime1,&num_prime_1);

   int prime2[100]; int num_prime_2;
   int msign2 = 1;
   // if (sum->denom < 0) { msign2 = -1; }
   sum->denom = abs(sum->denom);
   get_prime_factors(sum->denom,prime2,&num_prime_2);

   int  i = 0; int  j = 0;
   int z1 = prime1[i]; int z2 = prime2[j];

   while(i<num_prime_1 && j<num_prime_2)
   {
      if (z1==z2)
      {
         sum->numer = sum->numer/z1;
         sum->denom = sum->denom/z2;

         i  = i+1;
         j  = j+1;
         z1 = prime1[i];
         z2 = prime2[j];
      }
      else
      {
         if (z1>z2)
         {
            j = j+1;
            z2 = prime2[j];
         }
         else
         {
            i = i+1;
            z1 = prime1[i];
         }
      }
   }

   sum->numer = sum->numer*msign1;
   sum->denom = sum->denom;//*msign2;
}

void get_prime_factors(int n,
                       int prime_list[],
                       int* num_primes)
{
   *num_primes = 0;

   while (n%2==0)
   {
      prime_list[*num_primes] = 2;
      *num_primes = *num_primes + 1;
      n = n/2;
   }

   for (int i=3; i<=sqrt(n); i=i+2)
   {
      while (n%i==0)
      {
         prime_list[*num_primes] = i;
         *num_primes = *num_primes + 1;
         n = n/i;
      }
   }

   if (n>2)
   {
      prime_list[*num_primes] = n;
      *num_primes = *num_primes + 1;
   }
}
