#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void get_prime_factors(int n, int prm_list[], int* num_prms)
{
   printf(" I am in the **get_prime_factors** function.\n");
   *num_prms = 0;

   while (n%2==0)
   { prm_list[*num_prms] = 2; *num_prms=*num_prms+1; n=n/2; }

   for (int i=3; i<=sqrt(n); i=i+2)
   {
      while (n%i==0)
      {
         prm_list[*num_prms] = i;
         *num_prms = *num_prms + 1;
         n = n/i;
      }
   }
   if (n>2)
   { prm_list[*num_prms] = n; *num_prms = *num_prms + 1; }
}
