#include <stdio.h>
#include "fraction.h"
#include "properfraction.h"

void fraction_add(const fraction* a,
                  const fraction* b,
                  fraction* sum)
{
   printf("I am in the **fraction_add** function.\n");
   sum->denom = a->denom * b->denom;
   sum->numer = a->numer * b->denom + b->numer * a->denom;

   void fraction_reduce(fraction* sum);
   fraction_reduce(sum);
}
