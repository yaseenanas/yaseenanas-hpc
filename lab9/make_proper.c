#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "fraction.h"
#include "properfraction.h"

void make_proper(const fraction* in,
                 properfraction* out)
   {
     int n =1;
     if (in->numer <0 || in->denom <0)
     {
       n= -1;
     }

     if (abs(in->numer) < abs(in->denom))
     {

       out->whole= (in->numer)/(in->denom);

     }

     else if (abs(in->numer) > abs(in->denom))
     {
       out->whole= in->numer/in->denom;
       out->numer=(in->numer % in->denom)*n;
       out->denom= (in->denom);
     }
   }
