#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "fraction.h"
#include "properfraction.h"
int main()
{
   fraction a,b,sum,diff,prod,quot;
   properfraction sum_red,diff_red,prod_red,quot_red;

   a.numer = 1; a.denom = 10;
   b.numer = 5; b.denom = 3;

   void fraction_add(const fraction* a,
                     const fraction* b,
                     fraction* sum);
   fraction_add(&a,&b,&sum);



  void fraction_subtract(const fraction* a,
                       const fraction* b,
                       fraction* diff);
      fraction_subtract(&a,&b,&diff);


  void fraction_mult(const fraction* a,
                     const fraction* b,
                     fraction* prod);
        fraction_mult(&a,&b,&prod);


  void fraction_divide(const fraction* a,
                       const fraction* b,
                      fraction* quot);
        fraction_divide(&a,&b,&quot);

  void make_proper(const fraction* in, properfraction* out);
  make_proper(&sum,&sum_red);
  if (abs(sum_red.whole)>0)
  {
    printf("\n %i/%i  +  %i/%i  =  %i(%i/%i)\n\n",
           a.numer,a.denom,b.numer,b.denom,
           sum_red.whole,sum_red.numer,sum_red.denom);
  }

  else
  {
    printf("\n %i/%i  +  %i/%i  =  %i/%i\n\n",
           a.numer,a.denom,b.numer,b.denom,
           sum.numer,sum.denom);
  }

  void make_proper(const fraction* in, properfraction* out);
  make_proper(&diff,&diff_red);
  if (abs(diff_red.whole)>0)
  {
    printf("\n %i/%i  -  %i/%i  =  %i(%i/%i)\n\n",
           a.numer,a.denom,b.numer,b.denom,
           diff_red.whole,diff_red.numer,diff_red.denom);
  }

  else
  {
    printf("\n %i/%i  -  %i/%i  =  %i/%i\n\n",
           a.numer,a.denom,b.numer,b.denom,
           diff.numer,diff.denom);
  }

  void make_proper(const fraction* in, properfraction* out);
  make_proper(&prod,&prod_red);
  if (abs(prod_red.whole)>0)
  {
    printf("\n %i/%i  *  %i/%i  =  %i(%i/%i)\n\n",
           a.numer,a.denom,b.numer,b.denom,
           prod_red.whole,prod_red.numer,prod_red.denom);
  }

  else
  {
    printf("\n %i/%i  *  %i/%i  =  %i/%i\n\n",
           a.numer,a.denom,b.numer,b.denom,
           prod.numer,prod.denom);
  }

  void make_proper(const fraction* in, properfraction* out);
  make_proper(&quot,&quot_red);
  if (abs(quot_red.whole)>0)
  {
    printf("\n %i/%i  /  %i/%i  =  %i(%i/%i)\n\n",
           a.numer,a.denom,b.numer,b.denom,
           quot_red.whole,quot_red.numer,quot_red.denom);
  }

  else
  {
    printf("\n %i/%i  /  %i/%i  =  %i/%i\n\n",
           a.numer,a.denom,b.numer,b.denom,
           quot.numer,quot.denom);
  }

}
