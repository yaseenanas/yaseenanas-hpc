#
def LUfactor(A):
    import numpy as np
    from numpy import array
    from numpy import shape
    from numpy import identity
    U=A
    size=U.shape[0]
    n=size
    L= np.identity(size)
    for k in range(0,n-1):
        for j in range (k+1,n):
            L[j,k]=U[j,k]/U[k,k]
            for i in range(k,n):
                U[j,i]=U[j,i] - L[j,k]*U[k,i]
    return L,U

def determinant(A):
    import numpy as np
    from numpy import array
    from numpy import shape
    from numpy import identity
    L,U=LUfactor(A)
    n=U.shape[0]
    Udet=1
    for i in range(0,n):
        Udet=Udet*U[i,i]
        # U[i,i]=U[i,i]*U[i-1,i-1]
        # L[i,i]= L[i,i]*L[i-1,i-1]
    detA=1*Udet
    # detA= L[i,i]*U[i,i]
    return detA
