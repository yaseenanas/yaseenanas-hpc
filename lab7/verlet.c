#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void Verlet (const int whichrun,
            const double u0,
            const double v0,
            const double dt,
            const int NumSteps)
{
  double u[NumSteps+1];
  double v[NumSteps+1];

  //Initial Conditions
  u[0]=u0;
  v[0]=v0;

  // Main Loop
  for (int i=1;i<=NumSteps;i+=1)
  {
    double vs= v[i-1]+(dt/2)*(u[i-1]- pow(u[i-1],3));
    u[i]= u[i-1]+dt*vs;
    v[i]=vs+(dt/2)*(u[i]-pow(u[i],3));
  }

  //Print solution to file
  char filename[]= "outputX.data";
  filename[6]= whichrun+'0';
  FILE* outfile= fopen(filename,"w");
  for (int i=1;i<=NumSteps;i+=1)
  {
    fprintf(outfile,"%15f %15f\n",u[i],v[i]);
  }

  fclose(outfile);

}


#include <math.h>
#include <stdio.h>
#include <stdlib.h>
int main()
{
  // function declaration
void Verlet(const int whichrun,
            const double u0,
            const double v0,
            const double dt,
            const int NumSteps);
 const double Tfinal = 12.0;
 const int NumSteps = 50;
 const double dt= Tfinal/NumSteps;
 //run #1
 Verlet(1,1.0,0.4,dt,NumSteps);

 //run #2
 Verlet(2,1.0,0.6,dt,NumSteps);

 //run #3
 Verlet(3,1.0,0.8,dt,NumSteps);

 //run #4
 Verlet(4,-1.0,0.6,dt,NumSteps);

 //run #5
 Verlet(5,-1.0,0.4,dt,NumSteps);

 return 0;
}
