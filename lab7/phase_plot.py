def phase_plot(infile1,infile2,infile3,infile4,infile5):
    import numpy as np
    import matplotlib.pyplot as plt
    u1=[]
    v1= []
    u2=[]
    v2=[]
    u3=[]
    v3=[]
    u4=[]
    v4=[]
    u5=[]
    v5=[]

    with open(infile1) as f:
        for line in f:
            item1,item2= map(float,line.split())
            u1.append(item1)
            v1.append(item2)
    with open(infile2) as f:
        for line in f:
            item1,item2= map(float,line.split())
            u2.append(item1)
            v2.append(item2)
    with open(infile3) as f:
        for line in f:
            item1,item2= map(float,line.split())
            u3.append(item1)
            v3.append(item2)
    with open(infile4) as f:
        for line in f:
            item1,item2= map(float,line.split())
            u4.append(item1)
            v4.append(item2)
    with open(infile5) as f:
        for line in f:
            item1,item2= map(float,line.split())
            u5.append(item1)
            v5.append(item2)
    x1= np.array(u1)
    y1=np.array(v1)
    x2=np.array(u2)
    y2= np.array(v2)
    x3=np.array(u3)
    y3= np.array(v3)
    x4= np.array(u4)
    y4= np.array(v4)
    x5= np.array(u5)
    y5= np.array(v5)

    plt.plot(x1,y1,label= 'u=1 v=0.4')
    plt.plot(x2,y2,label= 'u=1 v=0.6')
    plt.plot(x3,y3,label= 'u= 1 v=0.8')
    plt.plot(x4,y4,label= 'u= -1 v=0.6')
    plt.plot(x5,y5,label= 'u= -1 v=0.4')
    plt.legend(loc=1)
    plt.title('Phase Plot')
    plt.savefig('PhasePlot.png', bbox_inches='tight')
    plt.show()

    return x1,y1,x2,y2,x3,y3,x4,y4,x5,y5


if __name__== "__main__":
    data1= 'output1.data'
    data2= 'output2.data'
    data3= 'output3.data'
    data4= 'output4.data'
    data5= 'output5.data'
    plot= phase_plot(data1,data2,data3,data4,data5)
