#include <stdio.h>
#include <stdlib.h>
#include "node.h"
#include <time.h>
#include <assert.h>

node* getfinal(struct node *cur)
{
  while (cur!= NULL && cur->next != NULL)
  {
    cur=cur->next;
  }
  return cur;
}
node *partition(node *head,node *end,
                 node **newhead, node **newend)
  {
    node *pivot= end;
    node *prev= NULL, *cur=head, *tail=pivot;
    while(cur != pivot)
    {
      if (cur->value < pivot->value)
      {
        if ((*newhead)==NULL)
        {
          (*newhead) = cur;
        }
        prev = cur;
        cur = cur->next;
      }
      else
      {
        if (prev)
        {
          prev->next=cur->next;
        }
        node *tmp=cur->next;
        cur->next= NULL;
        tail->next= cur;
        tail = cur;
        cur= tmp;
      }
    }

    if ((*newhead)==NULL)
    {
      (*newhead)= pivot;
    }
    (*newend)=tail;
    return pivot;
  }
//
  node *quicksortrecur(node *head,node *end)
  {
    if (!head || head == end)
    {
      return head;
    }
    node *newhead=NULL, *newend=NULL;
    node *pivot= partition(head,end,&newhead, &newend);
    if (newhead != pivot)
    {
      node *tmp= newhead;
      while (tmp->next != pivot)
      {
        tmp=tmp->next;
      }
      tmp->next = NULL;
      newhead= quicksortrecur(newhead,tmp);
      tmp=getfinal(newhead);
      tmp->next = pivot;
    }
    pivot->next=quicksortrecur(pivot->next,newend);
    return newhead;
  }
  
void SortList(node** head)
{

      *head= quicksortrecur(*head,getfinal(*head));
      return;
}
