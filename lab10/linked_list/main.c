#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "node.h"

void Swap2Nodes(node** head,int i,int j)
{
  // Nothing to do if x and y are same
  if (i == j)
      return;

  // Search for x
  node *ip = NULL, *ic = *head;
  while (ic &&  ic->value != i)
  {
      ip = ic;
      ic = ic->next;
  }

  // Search for y
  node *jp = NULL, *jc = *head;
  while (jc && jc->value != j)
  {
      jp = jc;
      jc = jc->next;
  }

  if (ip!=NULL)
  {
    ip->next=jc;
  }
  else
  {
    *head=jc;
  }

  if (jp!=NULL)
  {
    jp->next=ic;
  }
  else
  {
    *head=ic;
  }

  node* temp= jc->next;
  jc->next = ic->next;
  ic->next=temp;
}


int getnode(node* head,int index)
{
  // node* current = head;
      int count = 0;
      while (head != NULL)
      {
          if (count == index)
          {
            return (head->value);
          }
          count++;
          head = head->next;
      }
      assert(0);
}

int main()
{
   // Declare the head node in my list
   node* head = NULL;

   // Set number of nodes and generate a new list
   const int num_nodes = GetNumberOfNodes();
   GenerateList(&head,num_nodes);

   int i = getnode(head,1);
   int j = getnode (head,2);
   void Swap2Nodes(node** head,int i,int j);
   Swap2Nodes(&head,i,j);

   void SortList(node** head);
   SortList(&head);

   // Print list to screen
   Print(1,head); // foward print
   Print(0,head); // reverse print

   // Ask for a key, then search list
   if(num_nodes>0)
   {
      const int key = GetKey();
      SearchList(head,key);
   }

   // Delete list (free up memory)
   DeleteList(&head);
}
